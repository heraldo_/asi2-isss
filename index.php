<?php 
session_start();
  if(!empty($_POST))
  {
    require_once 'core/Login.php';
    $login = new Login();
    $login->init($_POST);
  }
?> 

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instituto Salvadoreño del seguro social</title>
    <!-- css -->    
    <link rel="stylesheet" href="resources/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="resources/css/style.css">

</head>
<body>
  <div class="contenedor">
    <header>
      <div class="Logo-isss">
        <div class="img-iss">
          <img src="resources/img/isss-logo.jpg" height="140" width="150">
        </div>
        <div class="text-logo">
          <p>INSTITUTO SALVADOREÑO</p>
          <p>DEL SEGURO SOCIAL</p>
        </div>
      </div>
      <div class="logo-sdpo">
        <img src="resources/img/log-sdpo.jpg" height="140" width="600">
      </div>   
    </header>

    <section class="ayuda">
      <i class="far fa-question-circle"></i>
    </section>

    <div class="contenido">
      <div class="menu">
        <h2 class="titulo">Marco Normativo</h2>
        <ul>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Actas de consejo</span>
          </li>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Ley pricipal que rige a la institución</span>
          </li>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Manuales básicos de orgsnización</span>
          </li>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Organigrama</span>
          </li>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Otros documentos normativos</span>
          </li>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Procedimientos y resultados de selección</span>
          </li>
          <li>
            <i class="fas fa-chevron-right"></i>
            <span>Reglamento de la Ley principal</span>
          </li>
        </ul>
      </div>
      <div class="formulario">
        <h1>Sign In</h1>
        <form class="contenido-formulario" action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
          <div class="contenido-input">
            <div class="form-group">
              <input type="text" name="email" class="form-control" id="nickname" placeholder="Username">
            </div>
            <div class="form-group">
              <input type="password" name="contrasenia" class="form-control" id="pass" placeholder="Password">
            </div>
          </div>
          <div class="conten-btn">
            <button type="submit" class="btn btn-danger" name="admin" value="admin">Administrador</button>
            <button type="submit" class="btn btn-primary" name="user" value="user">Usuario</button>
          </div>
          <br />
          <?php
            if(isset($_SESSION['ERROR'])){
              echo "<div class='alert alert-danger'>El correo o la contraseña son incorrectos</div>";
            }
            elseif(isset($_SESSION['PERMISO'])){
              echo "<div class='alert alert-warning'>No tienes acceso permitido</div>";
            }
          ?>
        </form>
      </div>
    </div>
  </div>
</body>
</html>