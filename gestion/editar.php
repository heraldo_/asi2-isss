<?php 
    require '../views/head-admin.php'; 
    require '../core/Gestion.php';
    $data = new Gestion();
    $id = $_GET['id'];
    $resultado = $data->show($id);
    $credenciales = $data->credenciales($id);

    if(!isset($resultado) && !isset($credenciales)){
        header("location: home.php");
    }

    if(isset($_POST['actualizar-credenciales'])){
        require_once '../core/Gestion.php';
        $data = new Gestion();
        $data->update($_POST);
        header("location: home.php");
    }
?>
<form id="formJornadas" method="post">
    <div class="contenedor-gestion-usuario">
        <div class="form-group" id="preview">
            <img src="<?php echo $resultado['imagen'] ?>" alt="..." class="img-thumbnail img-empleado" id="img">
        </div>
        <input type="text" value="<?php echo $_GET['id']?>" style="display:none" name="id" id="id">
        <div class="col-md-6 mb-3 d-flex">
            <input type="text" class="form-control" id="usuario" value="<?php echo $resultado['isss'] ?>" disabled>
        </div>
        <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="nombre"
                value="<?php echo $resultado['nombres'] . " " . $resultado['apellidos']  ?>" name="nombre" disabled>
        </div>
        <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="nickname" name="nickname" value="<?php echo $credenciales['nickname'] ?>">
            <div class="valid-feedback">correcto!</div>
            <div class="invalid-feedback">El nickname es requerido!</div>
        </div>
        <div class="d-flex align-items-start justify-content-start" style="width: 50%;">
            <div class="col-md-8">
                <input type="text" class="form-control" id="pass" name="pass" value="<?php echo $credenciales['pass'] ?>">
                <div class="valid-feedback">correcto!</div>
                <div class="invalid-feedback">La contraseña es requerida!</div>
            </div>
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="customSwitch1" name="estado" value="<?php echo $credenciales['estado'] ?>" onclick="std()"
                    <?php echo ($credenciales['estado'] == 1)? 'checked' : ''; ?>>
                <label class="custom-control-label" for="customSwitch1">inactivo / activo</label>
            </div>
        </div>
        <br>
        <div>
            <button class="btn btn-primary" type="submit" name="actualizar-credenciales">Aceptar</button>
            <a class="btn btn-danger" href="home">Cancelar</a>
        </div>
    </div>
</form>
<?php require '../views/footer-admin.php'; ?>
<script src="../resources/js/credenciales.js"></script>