<?php 
    require '../views/head-admin.php'; 
    require '../core/Gestion.php';
    $data = new Gestion();
    $id = $_GET['id'];
    $resultado = $data->show($id);
    $credenciales = $data->credenciales($id);

    if(!isset($resultado) && !isset($credenciales)){
        header("location: home.php");
    }
?>
<div class="contenedor-gestion-usuario">
    <div class="form-group" id="preview">
        <img src="<?php echo $resultado['imagen'] ?>" alt="..." class="img-thumbnail img-empleado" id="img">
    </div>
    <div class="col-md-6 mb-3 d-flex">
        <input type="text" class="form-control" id="usuario" value="<?php echo $resultado['isss'] ?>" disabled>
    </div>
    <div class="col-md-6 mb-3">
        <input type="text" class="form-control" id="nombre" value="<?php echo $resultado['nombres'] . " " . $resultado['apellidos']  ?>" name="nombre" disabled>
    </div>
    <div class="col-md-6 mb-3">
        <input type="text" class="form-control" id="nickname" value="<?php echo $credenciales['nickname'] ?>" disabled>
        <div class="valid-feedback">correcto!</div>
        <div class="invalid-feedback">El nickname es requerido!</div>
    </div>
    <div class="d-flex align-items-start justify-content-start" style="width: 50%;">
        <div class="col-md-8">
            <input type="text" class="form-control" id="pass" value="<?php echo $credenciales['pass'] ?>" disabled>
            <div class="valid-feedback">correcto!</div>
            <div class="invalid-feedback">La contraseña es requerida!</div>
        </div>
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="customSwitch1" name="estado" disabled 
            <?php echo ($credenciales['estado'] == 1)? 'checked' : ''; ?>>
            <label class="custom-control-label" for="customSwitch1">inactivo / activo</label>
        </div>
    </div>
    <br>
        <div>
            <a class="btn btn-primary" href="home">Cancelar</a>
        </div>
</div>
<?php require '../views/footer-admin.php'; ?>