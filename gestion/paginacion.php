<div class="content-GE">
    <h1>Gestion de empleados </h1>
    <br>
    <a class="btn btn-primary" href="crear">Nuevo Empleado</a>
    <br><br>
    <?php if (count($resultado) > 0) : ?>
    <table class="table table-bordered" style="text-align:center;">
        <thead class="thead-dark">
            <tr>
                <th>Fotografia</th>
                <th>Nombre Completo</th>
                <th>Código</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultado as $key): ?>
            <tr>
                <td><img src="<?php echo $key['imagen'] ?>" height="80" width="80"></td>
                <td><?php echo $key['nombres'] . " " . $key['apellidos']?></td>
                <td><?php echo $key['isss'] ?></td>
                <td>
                    <a class="btn btn-secondary" href="ver?id=<?php echo $key['idEmpleado']?>">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a class="btn btn-warning" href="editar?id=<?php echo $key['idEmpleado']?>">
                        <i class="far fa-edit"></i>

                    </a>
                    <!-- <a class="btn btn-danger" href="eliminar?id=<?php echo $key['idEmpleado']?>">
                        <i class="far fa-trash-alt"></i>
                    </a> -->
                </td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <?php endif ?>
    <br>
    <?php if ($cant_paginas > 1) : ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <?php if (isset($_GET['pagina'])) : ?>
            <?php if ($_GET['pagina'] != 1) : ?>
            <li class="page-item ">
                <a class="page-link" href="home?pagina=<?php echo $pagina - 1;?>">Anterior</a>
            </li>
            <li class="page-item"><a class="page-link"
                    href="home?pagina=<?php echo $pagina - 1;?>"><?php echo $pagina - 1; ?></a></li>
            <?php endif ?>
            <?php endif?>
            <li class="page-item active"><a class="page-link"
                    href="home?pagina=<?php echo $pagina; ?>"><?php echo $pagina; ?></a></li>
            <li class="page-item"><a class="page-link"
                    href="home?pagina=<?php echo $pagina + 1;?>"><?php echo $pagina + 1; ?></a></li>
            <li class="page-item">
                <a class="page-link" href="home?pagina=<?php echo $pagina + 1;?>">Siguiente</a>
            </li>
        </ul>
    </nav>
    <?php endif ?>
</div>