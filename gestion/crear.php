<?php 
    require '../views/head-admin.php';
    
    if(isset($_POST['almacenar-credenciales'])){
        require_once '../core/Gestion.php';
        $data = new Gestion();
        $data->crear($_POST);
        header("location: home.php");
    }
?>
<form id="formJornadas" method="post">
    <div class="contenedor-gestion-usuario">
        <div class="form-group" id="preview">
            <img src="../resources/img/usuarios/usuario.png" alt="..." class="img-thumbnail img-empleado" id="img">
        </div>
        <input type="text" value="5" style="display:none" name="id" id="id">
        <div class="col-md-6 mb-3 d-flex">
            <input type="text" class="form-control col-md-8" id="usuario" placeholder="Ingrese codigo de usuario">
            <input type="button" class="btn btn-primary" value="Buscar" style="margin-left:10px;" onclick="buscar()">
        </div>
        <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="nombre" placeholder="Nombre completo" name="nombre" required
                disabled>
        </div>
        <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="nickname" placeholder="Nickname" name="nickname" required>
            <div class="valid-feedback">correcto!</div>
            <div class="invalid-feedback">El nickname es requerido!</div>
        </div>
        <div class="d-flex align-items-start justify-content-start" style="width: 50%;">
            <div class="col-md-8">
                <input type="text" class="form-control" id="pass" placeholder="Ingrese contraseña" name="pass" required>
                <div class="valid-feedback">correcto!</div>
                <div class="invalid-feedback">La contraseña es requerida!</div>
            </div>
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="estado" name="estado" value="0" onclick="std()"> 
                <label class="custom-control-label" for="estado">inactivo / activo</label>
            </div>
        </div>
        <br>
        <div>
            <button class="btn btn-primary" type="submit" name="almacenar-credenciales">Aceptar</button>
            <a class="btn btn-danger" href="home">Cancelar</a>
        </div>
    </div>
</form>
<?php require '../views/footer-admin.php'; ?>
<script src="../resources/js/credenciales.js"></script>