<?php 

    if(isset($_POST['crear-jornada'])){
        require_once '../core/Jornadas.php';
        $data = new Jornadas();
        $data->save($_POST);
        header("location: home.php");
    }
 
    require '../views/head-admin.php';

?>
<div class="titulo-jornada">
    <img src="../resources/img/isss-logo.jpg" height="120" width="120">
    <h2 class="cjl-jornada">Crear jornada laboral</h2>
</div>
<form id="formJornadas" method="post">
    <div class="form-row formulario-jornada-nueva">
        <div class="col-md-9 form-letf-file">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Nombre de la jornada</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduzca nombre de la jornada"
                        required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">EL nombre de la jornada es requerido!</div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese una descripción">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2 mb-3">
                    <label>Hora inicio</label>
                    <input type="time" name="hora-inicio" class="form-control" id="horaInicio" required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">La hora de inicio es requerida!</div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2 mb-3">
                    <label>Hora fin</label>
                    <input type="time" name="hora-fin" class="form-control" id="horaFin" required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">La hora de fin es requerida!</div>
                </div>
            </div>
        </div>
    </div>
    <div class="botones-form-jornada">
        <input class="btn btn-warning" type="button" value="Limpiar" onclick="limpiarCamposjornada()" >
        <div class="btn-right">
            <button class="btn btn-success" type="submit" onclick="validarCamposjornada()" name="crear-jornada">Crear</button>
            <a class="btn btn-danger" href="home">Cancelar</a>
        </div>
    </div>
</form>
<?php require '../views/footer-admin.php';?>