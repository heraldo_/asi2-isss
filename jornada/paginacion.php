<div class="content-GE">
    <h1>Gestion de jornadas laborales</h1>
    <br>
    <a class="btn btn-primary" href="crear">Crear jornada</a>
    <br><br>
    <table class="table table-bordered" style="text-align:center;">
        <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Nombre de la jornada</th>
                <th>Descripción</th>
                <th>Hora de inicio</th>
                <th>Hora de fin</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultado as $key): ?>
            <tr>
                <td><?php echo $key['id-jornada'] ?></td>
                <td><?php echo $key['nombreJornada'] ?></td>
                <td><?php echo $key['descripcion'] ?></td>
                <td><?php echo $key['horaInicio'] ?></td>
                <td><?php echo $key['horaFinal'] ?></td>
                <td>
                    <a class="btn btn-warning" href="editar?id=<?php echo $key['id-jornada']?>">
                        <i class="far fa-edit"></i>
                        
                    </a>
                    <a class="btn btn-danger" href="eliminar?id=<?php echo $key['id-jornada']?>">
                        <i class="far fa-trash-alt"></i>
                        
                    </a>
                </td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <br>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <?php if (isset($_GET['pagina'])) : ?>
            <?php if ($_GET['pagina'] != 1) : ?>
            <li class="page-item ">
                <a class="page-link" href="home?pagina=<?php echo $pagina - 1;?>">Anterior</a>
            </li>
            <li class="page-item"><a class="page-link"
                    href="home?pagina=<?php echo $pagina - 1;?>"><?php echo $pagina - 1; ?></a></li>
            <?php endif ?>
            <?php endif?>
            <li class="page-item active"><a class="page-link" href="home?pagina=<?php echo $pagina; ?>"><?php echo $pagina; ?></a></li>
            <li class="page-item"><a class="page-link"
                    href="home?pagina=<?php echo $pagina + 1;?>"><?php echo $pagina + 1; ?></a></li>
            <li class="page-item">
                <a class="page-link" href="home?pagina=<?php echo $pagina + 1;?>">Siguiente</a>
            </li>
        </ul>
    </nav>
</div>