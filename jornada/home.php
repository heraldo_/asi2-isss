<?php
    require '../views/head-admin.php';
    require_once '../core/VConexion.php';

    $conectar = new VConexion();
    $db = $conectar->conectar();
    
    if(!isset($_GET['pagina'])){
        $pagina = 1;
    }else{
        $pagina = $_GET['pagina'];
    }

    if($pagina <= 0){
        header('location: home');
    }

    $por_pagina = 7;
    $sql = $db->prepare('SELECT COUNT(*) as "total" FROM jornada');
    $sql->execute();
    $total_paginas = $sql->fetch()['total'];
    $cant_paginas = ceil($total_paginas / $por_pagina);

    if($pagina >= $cant_paginas){
    $pagina = $cant_paginas;
    }
    $pag_siguiente = $pagina * $por_pagina - $por_pagina;

    $sql1 = $db->prepare("SELECT * FROM jornada LIMIT $pag_siguiente, $por_pagina");
    $sql1->execute();
    $resultado = $sql1->fetchAll();

    require 'paginacion.php';
    require '../views/footer-admin.php';
?>