<?php
session_start();
if($_SESSION['rol'] != "user"){
    session_destroy();
    header("location: index.php");
}
if(!empty($_POST))
{
    session_destroy();
    header("location: /asi2-isss");
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>dashboard</title>

    <!-- css -->
    <link rel="stylesheet" href="../resources/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/css/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../resources/css/user.css">
</head>

<body>
    <header>
        <p class="rol">USUARIO</p>
        <div class="search">
            <form class="contenido-formulario" action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
                <input type="text" name="email" style="visibility: hidden;">
                <button type="submit" class="btn btn-primary btnpry" name="salir">Salir</button>
                <i class="fas fa-search"></i>
            </form>
        </div>
    </header>
    <div class="contenido">
        <div class="menu">
            <div class="home">
                <a class="hvr-grow" href="/asi2-isss/views/user.php">
                    <i class="fas fa-home"></i>
                    <span>INICIO</span>
                </a>
            </div>
            <div class="options">
                <div class="hvr-grow" data-toggle="collapse" href="#gestionUsuarios" aria-expanded="false"
                    aria-controls="gestionUsuarios">
                    <a href="../usuario/ver.php">
                        <i class="far fa-user"></i>
                        <span>Perfil</span>
                    </a>
                </div>
            </div>
            <div class="options">
                <div class="hvr-grow" data-toggle="collapse" href="#gestionEmpleados" aria-expanded="false"
                    aria-controls="gestionEmpleados">
                    <i class="fas fa-chevron-down"></i>
                    <span>Permisos</span>
                </div>
                <div class="collapse" id="gestionEmpleados">
                    <a class="icono hvr-grow">
                        <i class="fas fa-users"></i>
                        <span> Crear permiso</span>
                    </a>
                    <a class="icono hvr-grow">
                        <i class="fas fa-envelope"></i>
                        <span class="solicitud">Solicitudes</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="escritorio" id="escritorio">