<?php 
    session_start();

    if($_SESSION['rol'] != "admin"){
        session_destroy();
        header("location: /asi2-isss");
    }
    
    if(isset($_POST['salir']))
    {
        session_destroy();
        header("location: /asi2-isss");
    }
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>dashboard</title>

    <!-- css -->
    <link rel="stylesheet" href="../resources/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/css/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>

<body>
    <header>
        <p class="rol">ADMINISTRADOR</p>
        <div class="search">
            <form class="contenido-formulario" action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
                <!-- <a href="#" class="btn btn-clean">
                    <span class="notify">8</span>
                    <i class="far fa-bell"></i>
                </a> -->
                <input type="text" name="email" style="visibility: hidden;">
                <button type="submit" class="btn btn-primary btnpry" name="salir">Salir</button>
            </form>
        </div>
    </header>
    <div class="contenido">
        <div class="menu">
            <div class="home">
                <a class="hvr-grow" href="/asi2-isss/views/admin.php">
                    <i class="fas fa-home"></i>
                    <span>INICIO</span>
                </a>
            </div>
            <div class="options">
                <div class="hvr-grow" data-toggle="collapse" href="#gestionUsuarios" aria-expanded="false"
                    aria-controls="gestionUsuarios">
                    <i class="fas fa-chevron-down"></i>
                    <span>Gestion Usuarios</span>
                </div>
                <div class="collapse" id="gestionUsuarios">
                    <a class="icono hvr-grow" href="../gestion/home.php">
                        <i class="fa fa-user"></i>
                        <span> Gestion Usuarios</span>
                    </a>
                    <div class="icono hvr-grow">
                        <i class="fa fa-circle-notch"></i>
                        <span> Asignar Roles</span>
                    </div>
                </div>
            </div>
            <div class="options">
                <div class="hvr-grow" data-toggle="collapse" href="#gestionEmpleados" aria-expanded="false"
                    aria-controls="gestionEmpleados">
                    <i class="fas fa-chevron-down"></i>
                    <span>Gestion Empleados</span>
                </div>
                <div class="collapse" id="gestionEmpleados">
                    <a class="icono hvr-grow" href="../empleado/home.php">
                        <i class="fas fa-users"></i>
                        <span> Empleados</span>
                    </a>
                    <a class="icono hvr-grow" href="../jornada/home.php">
                        <i class="fas fa-users"></i>
                        <span> Jornadas Laborales</span>
                    </a>
                    <a class="icono hvr-grow" href="../permisos/home.php">
                        <i class="fas fa-users"></i>
                        <span> Tipo de permisos</span>
                    </a>
                </div>
            </div>
            <div class="solicitudes hvr-grow">
                <i class="fas fa-envelope"></i>
                <span>Solicitudes de permisos</span>
            </div>
        </div>
        <div class="escritorio" id="escritorio">