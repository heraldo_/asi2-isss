-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-10-2019 a las 17:19:02
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `permisos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_credenciales`
--

CREATE TABLE `cat_credenciales` (
  `idCredenciales` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `fk_empleado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cat_credenciales`
--

INSERT INTO `cat_credenciales` (`idCredenciales`, `nickname`, `pass`, `rol`, `estado`, `fk_empleado`) VALUES
(1, 'maxcampos', '1234567', 'admin', 1, 8),
(5, 'alexis', '12345', 'user', 1, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_departamento`
--

CREATE TABLE `cat_departamento` (
  `idDepartamento` int(11) NOT NULL,
  `nombreDepartamento` varchar(100) NOT NULL,
  `fk_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_departamento`
--

INSERT INTO `cat_departamento` (`idDepartamento`, `nombreDepartamento`, `fk_pais`) VALUES
(1, 'Ahuachapan', 1),
(2, 'Santa Ana', 1),
(3, 'Sonsonate', 1),
(4, 'Chalatenango', 1),
(5, 'La Libertad', 1),
(6, 'San Salvador', 1),
(7, 'Cuscatlan', 1),
(8, 'La Paz', 1),
(9, 'Cabañas', 1),
(10, 'San Vicente', 1),
(11, 'Usulutan', 1),
(12, 'San Miguel', 1),
(13, 'Morazan', 1),
(14, 'La Union', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_municipio`
--

CREATE TABLE `cat_municipio` (
  `idMunicipio` int(11) NOT NULL,
  `nombreMunicipio` varchar(100) NOT NULL,
  `fk_departamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_municipio`
--

INSERT INTO `cat_municipio` (`idMunicipio`, `nombreMunicipio`, `fk_departamento`) VALUES
(1, 'Ahuachapan', 1),
(2, 'Apaneca', 1),
(3, 'Atiquizaya', 1),
(4, 'Concepcion de ataco', 1),
(5, 'El Refugio', 1),
(6, 'Guayamango', 1),
(7, 'Jujutla', 1),
(8, 'San Francisco Menendez', 1),
(9, 'San Lorenzo', 1),
(10, 'San Pedro Puxtla', 1),
(11, 'Turin', 1),
(12, 'Candelaria de la Frontera', 2),
(13, 'Chalchuapa', 2),
(14, 'Coatepeque', 2),
(15, 'El Congo', 2),
(16, 'El Porvenir', 2),
(17, 'Masahuat', 2),
(18, 'Metapan', 2),
(19, 'San Antonio Pajonal', 2),
(20, 'San Sebastian Salitrillo', 2),
(21, 'Santa Ana', 2),
(22, 'Santa Rosa Guachipilin', 2),
(23, 'Santiago de la frontera', 2),
(24, 'Texistepeque', 2),
(25, 'Acajutla', 3),
(26, 'Armenia', 3),
(27, 'Caluco', 3),
(28, 'Cuisnahuat', 3),
(29, 'Izalco', 3),
(30, 'Juayua', 3),
(31, 'Nahuizalco', 3),
(32, 'Nahulingo', 3),
(33, 'Salcoatitan', 3),
(34, 'San Antonio del Monte', 3),
(35, 'San Julian', 3),
(36, 'Santa Catarina Masahuat', 3),
(37, 'Santa Isabel Ishuatan', 3),
(38, 'Santo Domingo Guzman', 3),
(39, 'Sonsonate', 3),
(40, 'Sonzacate', 3),
(41, 'Agua Caliente', 4),
(42, 'Arcatao', 4),
(43, 'Azacualpa', 4),
(44, 'Chalatenango', 4),
(45, 'Comalapa', 4),
(46, 'Citala', 4),
(47, 'Concepcion Quezaltepeque', 4),
(48, 'Dulce Nombre de Maria', 4),
(49, 'El Carrizal', 4),
(50, 'El Paraiso', 4),
(51, 'La Laguna', 4),
(52, 'La Palma', 4),
(53, 'La Reina', 4),
(54, 'Las Vueltas', 4),
(55, 'Nueva Concepcion', 4),
(56, 'Nueva Trinidad', 4),
(57, 'Nombre de Jesus', 4),
(58, 'Ojos de Agua', 4),
(59, 'Potonico', 4),
(60, 'San Antonio de la cruz', 4),
(61, 'San Antonio Los Ranchos', 4),
(62, 'San Fernando', 4),
(63, 'San Francisco Lempa', 4),
(64, 'San Francisco Morazan', 4),
(65, 'San Ignacio', 4),
(66, 'San Isidro Labrador', 4),
(67, 'San Jose Cascasque', 4),
(68, 'San Jose las Flores', 4),
(69, 'San Luis del Carmen', 4),
(70, 'San Miguel de Mercedes', 4),
(71, 'San Rafael', 4),
(72, 'Santa Rita', 4),
(73, 'Tejutla', 4),
(74, 'Antiguo Cuscatlan', 5),
(75, 'Chiltiupan', 5),
(76, 'Ciudad Arce', 5),
(77, 'Colon', 5),
(78, 'Comasagua', 5),
(79, 'Huizuacar', 5),
(80, 'Jayaque', 5),
(81, 'Jicalapa', 5),
(82, 'La Libertad', 5),
(83, 'Santa Tecla', 5),
(84, 'Nuevo Cuscatlan', 5),
(85, 'San Juan Opico', 5),
(86, 'Quezaltepeque', 5),
(87, 'Sacacoyo', 5),
(88, 'San Jose Villanueva', 5),
(89, 'San Matias', 5),
(90, 'San Pablo Tacachico', 5),
(91, 'Talnique', 5),
(92, 'Tamanique', 5),
(93, 'Teotepeque', 5),
(94, 'Tepecoyo', 5),
(95, 'Zaragoza', 5),
(96, 'Aguilares', 6),
(97, 'Apopa', 6),
(98, 'Ayutuxtepeque', 6),
(99, 'Cuscatancingo', 6),
(100, 'Ciudad Delgado', 6),
(101, 'El Paisnal', 6),
(102, 'Guazapa', 6),
(103, 'Ilopango', 6),
(104, 'Mejicanos', 6),
(105, 'Nejapa', 6),
(106, 'Panchimalco', 6),
(107, 'Rosario de Mora', 6),
(108, 'San Marcos', 6),
(109, 'San Martin', 6),
(110, 'San Salvador', 6),
(111, 'Santiago Texacuangos', 6),
(112, 'Santo Tomas', 6),
(113, 'Soyapango', 6),
(114, 'Tonacatepeque', 6),
(115, 'Candelaria', 7),
(116, 'Cojutepeque', 7),
(117, 'El Carmen', 7),
(118, 'El Rosario', 7),
(119, 'Monte San Juan', 7),
(120, 'Oratorio de Concepcion', 7),
(121, 'San Batolome Perulapia', 7),
(122, 'San Cristobal', 7),
(123, 'San Jose Guayabal', 7),
(124, 'San Pedro Perulapan', 7),
(125, 'San Rafael Cedros', 7),
(126, 'San Ramon', 7),
(127, 'Santa Cruz Analquito', 7),
(128, 'Santa Cruz Michapa', 7),
(129, 'Suchitoto', 7),
(130, 'Tenancingo', 7),
(131, 'Cuyultitan', 8),
(132, 'El Rosario', 8),
(133, 'Jerusalen', 8),
(134, 'Mercedes de la Ceiba', 8),
(135, 'Olocuilta', 8),
(136, 'Paraiso de Osorio', 8),
(137, 'San Antonio Masahuat', 8),
(138, 'San Emigdio', 8),
(139, 'San Francisco Chinameca', 8),
(140, 'San Juan Nonualco', 8),
(141, 'San Juan Talpa', 8),
(142, 'San Juan Tepezontes', 8),
(143, 'San Luis Talpa', 8),
(144, 'San Luis la Herradura', 8),
(145, 'San Miguel Tepezontes', 8),
(146, 'San Pedro Masahut', 8),
(147, 'San Pedro Nonualco', 8),
(148, 'San Rafael Obrajuelo', 8),
(149, 'Santa Maria Ostuma', 8),
(150, 'Santiago Nonualco', 8),
(151, 'Tapalhuaca', 8),
(152, 'Zacatecoluca', 8),
(153, 'Cinquera', 9),
(154, 'Dolores', 9),
(155, 'Guacotecti', 9),
(156, 'Ilobasco', 9),
(157, 'Jutiapa', 9),
(158, 'San Isidro', 9),
(159, 'Sensuntepeque', 9),
(160, 'Tejutepeque', 9),
(161, 'Victoria', 9),
(162, 'Apastepeque', 10),
(163, 'Guadalupe', 10),
(164, 'San Cayetano Istepeque', 10),
(165, 'San Esteban Catarina', 10),
(166, 'San Ildefonso', 10),
(167, 'San Lorenzo', 10),
(168, 'San Sebastian', 10),
(169, 'San Vicente', 10),
(170, 'Santa Clara', 10),
(171, 'Santo Domingo', 10),
(172, 'Tecoluca', 10),
(173, 'Tepetitan', 10),
(174, 'Verapaz', 10),
(175, 'Alegria', 11),
(176, 'Berlin', 11),
(177, 'California', 11),
(178, 'Concepcion Batres', 11),
(179, 'El Triunfo', 11),
(180, 'Ereguayquin', 11),
(181, 'Estanzuelas', 11),
(182, 'Jiquilisco', 11),
(183, 'Jucuapa', 11),
(184, 'Jucuaran', 11),
(185, 'Mercedes Umaña', 11),
(186, 'Nueva Granda', 11),
(187, 'Ozatlan', 11),
(188, 'Puerto el Triunfo', 11),
(189, 'San Agustin', 11),
(190, 'San Buenaaventura', 11),
(191, 'San Dionicio', 11),
(192, 'San Francisco Javier', 11),
(193, 'Santa Elena', 11),
(194, 'Santa Maria', 11),
(195, 'Santiago de Maria', 11),
(196, 'Tecapan', 11),
(197, 'Usulutan', 11),
(198, 'Carolina', 12),
(199, 'Chapeltique', 12),
(200, 'Chinameca', 12),
(201, 'Chirilagua', 12),
(202, 'Ciudad Barrios', 12),
(203, 'Comacaran', 12),
(204, 'El Transito', 12),
(205, 'Lolotique', 12),
(206, 'Moncagua', 12),
(207, 'Nueva Guadalupe', 12),
(208, 'Nuevo Eden de San Juan', 12),
(209, 'Quelepa', 12),
(210, 'San Antonio del Mosco', 12),
(211, 'San Gerardo', 12),
(212, 'San Jorge', 12),
(213, 'San Luis de la Reina', 12),
(214, 'San Miguel', 12),
(215, 'San Rafael Oriente', 12),
(216, 'Sesoi', 12),
(217, 'Uluazapa', 12),
(218, 'Arambala', 13),
(219, 'Cacaopera', 13),
(220, 'Chilanga', 13),
(221, 'Corinto', 13),
(222, 'Delicias de Concepcion', 13),
(223, 'El Divisadero', 13),
(224, 'EL Rosario', 13),
(225, 'Gualococti', 13),
(226, 'Guatajiagua', 13),
(227, 'Joateca', 13),
(228, 'Jocoaitique', 13),
(229, 'Jocoro', 13),
(230, 'Lolotiquillo', 13),
(231, 'Meanguera', 13),
(232, 'Osicala', 13),
(233, 'Perquin', 13),
(234, 'San Carlos', 13),
(235, 'San Fernando', 13),
(236, 'San Francisco Gotera', 13),
(237, 'San Isidro', 13),
(238, 'San Simon', 13),
(239, 'Sensembra', 13),
(240, 'Sociedad', 13),
(241, 'Torola', 13),
(242, 'Yamabal', 13),
(243, 'Yoloiquin', 13),
(244, 'anamoros', 14),
(246, 'Bolivar', 14),
(247, 'Concepcion de Oriente', 14),
(248, 'Conchagua', 14),
(249, 'El Carmen', 14),
(250, 'El Sauce', 14),
(251, 'Intipuca', 14),
(252, 'La Union', 14),
(253, 'Lislique', 14),
(254, 'Meanguera del Golfo', 14),
(255, 'Nueva Esparta', 14),
(256, 'Pasaquina', 14),
(257, 'Poloros', 14),
(258, 'San Alejo', 14),
(259, 'San Jose', 14),
(260, 'Santa Rosa de Lima', 14),
(261, 'Yayantique', 14),
(262, 'Yucuaiquin', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_pais`
--

CREATE TABLE `cat_pais` (
  `idPais` int(11) NOT NULL,
  `nombrePais` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_pais`
--

INSERT INTO `cat_pais` (`idPais`, `nombrePais`) VALUES
(1, 'El Salvador'),
(2, 'Guatemala');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `estadoCivil` varchar(25) NOT NULL,
  `pais` varchar(25) NOT NULL,
  `fechaDeNacimiento` varchar(12) NOT NULL,
  `departamento` varchar(25) NOT NULL,
  `telefono` varchar(25) NOT NULL,
  `ciudad` varchar(25) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `dui` varchar(10) NOT NULL,
  `nit` varchar(17) NOT NULL,
  `afp` varchar(10) NOT NULL,
  `isss` varchar(8) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `contrato` varchar(30) NOT NULL,
  `fechaContrato` varchar(12) NOT NULL,
  `jornada` int(11) NOT NULL,
  `cargo` varchar(25) NOT NULL,
  `salario` varchar(10) NOT NULL,
  `dependencia` varchar(50) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `activo` varchar(1) NOT NULL DEFAULT 'S'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `apellidos`, `nombres`, `estadoCivil`, `pais`, `fechaDeNacimiento`, `departamento`, `telefono`, `ciudad`, `correo`, `dui`, `nit`, `afp`, `isss`, `direccion`, `contrato`, `fechaContrato`, `jornada`, `cargo`, `salario`, `dependencia`, `imagen`) VALUES
(8, 'Campos', 'Max', 'Soltero', '1', '05/27/1993', '11', '79394323', '176', 'max.elicampos14@hotmail.es', '1234567', '123-52', '45645645', '120345', 'Col. Bolivar', '24', '10/22/2019', 25, 'publico', '400.00', 'san bartolo', '../resources/img/usuarios/imagen_23074127.jpg'),
(12, 'Alexis', 'Vasquez', 'soltero', '1', '10/27/1993', '6', '76364323', '110', 'max.ajskd', 'aasdasd', 'asdasdasd', 'asdasdasd', 'Mc123', 'asdasdasdad', 'Turno completo', '10/24/2019', 24, 'asdfasdf', 'adfadf', 'afasf', '../resources/img/usuarios/imagen_24163103.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE `jornada` (
  `id-jornada` int(11) NOT NULL,
  `nombreJornada` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFinal` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `jornada`
--

INSERT INTO `jornada` (`id-jornada`, `nombreJornada`, `descripcion`, `horaInicio`, `horaFinal`) VALUES
(24, 'Turno completo', '', '08:00:00', '16:00:00'),
(25, 'Doble turno', '', '08:00:00', '00:00:00'),
(26, 'Medio turno maÃ±ana', '', '08:00:00', '12:00:00'),
(27, 'Medio turno tarde', '', '12:00:00', '16:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `idPermisos` int(11) NOT NULL,
  `nombrePermiso` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `tiempo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`idPermisos`, `nombrePermiso`, `descripcion`, `tiempo`) VALUES
(6, 'Enfermedad comun', '', '2 dias'),
(7, 'Enfermedad profecional', '', '4 dias'),
(8, 'Accidente comun', '', '2 dias'),
(9, 'Accidente de trabajo', '', '5 dias'),
(10, 'Maternidad', '', '84 dias'),
(11, 'Delegacion deportiva', '', '1 dia'),
(12, 'Seminario o congreso', '', '2 dias'),
(13, 'Cargo publico', '', '5 dias'),
(14, 'Tesis de graduacion', '', '3 dias'),
(15, 'Motivos especiales', '', '1'),
(16, 'Mision Oficial', '', '15 dias'),
(17, 'Cita medica', '', '1 dia'),
(18, 'Citacion publica', '', '2 dias'),
(19, 'Beca', '', '1 dia'),
(20, 'Matrimonio', '', '5 dias'),
(21, 'Duelo', '', '3 dias'),
(22, 'Motivo personal', '', '1 dia'),
(23, 'Media jornada', '', '4 horas'),
(24, 'Otros(explique)', '', 'indefinido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `contrasenia` varchar(20) NOT NULL,
  `rol` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `nombre`, `correo`, `contrasenia`, `rol`) VALUES
(1, 'Max', 'max.elicampos14@hotmail.es', 'campos123', 'admin'),
(2, 'Alex', 'max.vcampos@outlook.com', 'vasquez123', 'user');

--
-- Ýndices para tablas volcadas
--

--
-- Indices de la tabla `cat_credenciales`
--
ALTER TABLE `cat_credenciales`
  ADD PRIMARY KEY (`idCredenciales`),
  ADD KEY `fk_empleado` (`fk_empleado`);

--
-- Indices de la tabla `cat_departamento`
--
ALTER TABLE `cat_departamento`
  ADD PRIMARY KEY (`idDepartamento`),
  ADD KEY `fk_pais` (`fk_pais`);

--
-- Indices de la tabla `cat_municipio`
--
ALTER TABLE `cat_municipio`
  ADD PRIMARY KEY (`idMunicipio`),
  ADD KEY `fk_departamento` (`fk_departamento`);

--
-- Indices de la tabla `cat_pais`
--
ALTER TABLE `cat_pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`),
  ADD KEY `jornada` (`jornada`);

--
-- Indices de la tabla `jornada`
--
ALTER TABLE `jornada`
  ADD PRIMARY KEY (`id-jornada`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`idPermisos`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cat_credenciales`
--
ALTER TABLE `cat_credenciales`
  MODIFY `idCredenciales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cat_departamento`
--
ALTER TABLE `cat_departamento`
  MODIFY `idDepartamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `cat_municipio`
--
ALTER TABLE `cat_municipio`
  MODIFY `idMunicipio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;

--
-- AUTO_INCREMENT de la tabla `cat_pais`
--
ALTER TABLE `cat_pais`
  MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `jornada`
--
ALTER TABLE `jornada`
  MODIFY `id-jornada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `idPermisos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cat_credenciales`
--
ALTER TABLE `cat_credenciales`
  ADD CONSTRAINT `cat_credenciales_ibfk_1` FOREIGN KEY (`fk_empleado`) REFERENCES `empleado` (`idEmpleado`);

--
-- Filtros para la tabla `cat_departamento`
--
ALTER TABLE `cat_departamento`
  ADD CONSTRAINT `cat_departamento_ibfk_1` FOREIGN KEY (`fk_pais`) REFERENCES `cat_pais` (`idPais`);

--
-- Filtros para la tabla `cat_municipio`
--
ALTER TABLE `cat_municipio`
  ADD CONSTRAINT `cat_municipio_ibfk_1` FOREIGN KEY (`fk_departamento`) REFERENCES `cat_departamento` (`idDepartamento`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`jornada`) REFERENCES `jornada` (`id-jornada`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
