<?php 

    if(isset($_POST['crear-permiso'])){
        require_once '../core/Permisos.php';
        $data = new Permisos();
        $data->save($_POST);
        header("location: home.php");
    }

    require '../views/head-admin.php';

?>
<div class="titulo-permiso">
    <img src="../resources/img/isss-logo.jpg" height="120" width="120">
    <h2 class="cjl-permiso">Crear Tipo permiso</h2>
</div>
<form id="formPermisos" method="post">
    <div class="form-row formulario-permiso-nuevo">
        <div class="col-md-9 form-letf-file">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Nombre del permiso</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduzca nombre del permiso"
                        required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">EL nombre del permiso es requerido!</div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese una descripción">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Tiempo</label>
                    <input type="text" class="form-control" name="tiempo" id="tiempo" placeholder="Ingrese duración del permiso"
                    required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">La duración del permiso es requerido!</div>
                </div>
            </div>
        </div> 
    </div>
    <div class="botones-form-permiso">
        <input class="btn btn-warning" type="button" value="Limpiar" onclick="limpiarCamposPermiso()" >
        <div class="btn-right">
            <button class="btn btn-success" type="submit" onclick="validarCamposPermiso()" name="crear-permiso">Crear</button>
            <a class="btn btn-danger" href="home">Cancelar</a>
        </div>
    </div>
</form>
<?php require '../views/footer-admin.php';?>