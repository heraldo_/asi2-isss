<?php 

    require_once '../core/Permisos.php';

    $data = new Permisos();
    $id = $_GET['id'];
    $resultado = $data->show($id);

    if(isset($resultado)){
        
    }else{
        header("location: home.php");
    }

    if(isset($_POST['editar-permiso'])){
        $data->update($_POST, $id);
        header("location: home.php");
    }

    require '../views/head-admin.php';

?>

<div class="titulo-permiso">
    <img src="../resources/img/isss-logo.jpg" height="120" width="120">
    <h2 class="cjl-permiso">Crear Tipo permiso</h2>
</div>
<form id="formPermisos" method="post">
    <div class="form-row formulario-permiso-nuevo">
        <div class="col-md-9 form-letf-file">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Nombre del permiso</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduzca nombre del permiso"
                    value="<?php echo $resultado['nombrePermiso'] ?>" required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">EL nombre del permiso es requerido!</div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese una descripción"
                    value="<?php echo $resultado['descripcion'] ?>">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>Tiempo</label>
                    <input type="text" class="form-control" name="tiempo" id="tiempo" placeholder="Ingrese duración del permiso"
                    value="<?php echo $resultado['tiempo'] ?>" required>
                    <div class="valid-feedback">correcto!</div>
                    <div class="invalid-feedback">La duración del permiso es requerido!</div>
                </div>
            </div>
        </div> 
    </div>
    <div class="botones-form-permiso">
        <div class="btn-right">
            <button class="btn btn-success" type="submit" onclick="validarCamposPermiso()" name="editar-permiso">Actualizar</button>
            <a class="btn btn-danger" href="home">Cancelar</a>
        </div>
    </div>
</form>
<?php require '../views/footer-admin.php';?>