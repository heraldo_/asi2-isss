const formularioEmpleado = [
    '#apellidos', 
    '#nombres',
    '#estadoCivil',
    '#pais',
    '#calendario',
    '#departamento',
    '#telefono',
    '#ciudad',
    '#correo',
    '#dui',
    '#nit',
    '#afp',
    '#isss',
    '#direccion',
    '#contrato',
    '#fechaContrato',
    '#jornada',
    '#cargo',
    "#salario",
    "#dependencia"
];

function validarCampos(){
    var i = 0;
    for(i; i < formularioEmpleado.length; i++){
        $(formularioEmpleado[i]).val().length > 0 ? $(formularioEmpleado[i]).removeClass("is-invalid").addClass("is-valid") : $(formularioEmpleado[i]).removeClass("is-valid").addClass("is-invalid");
    }
}

function limpiar(){
    var i = 0;
    for(i; i < formularioEmpleado.length; i++){
        $(formularioEmpleado[i]).removeClass("is-invalid").removeClass("is-valid");
    }
    document.getElementById("general").reset();
    document.getElementById("trabajo").reset();
}

const formularioJornada = [
    '#nombre',
    '#horaInicio',
    '#horaFin'
];

function validarCamposjornada(){
    var i = 0;
    for(i; i < formularioJornada.length; i++){
        $(formularioJornada[i]).val().length > 0 ? $(formularioJornada[i]).removeClass("is-invalid").addClass("is-valid") : $(formularioJornada[i]).removeClass("is-valid").addClass("is-invalid");
    }
}

function limpiarCamposjornada(){
    var i = 0;
    for(i; i < formularioJornada.length; i++){
        $(formularioJornada[i]).removeClass("is-invalid").removeClass("is-valid");
    }
    document.getElementById("formJornadas").reset();
}

const formularioPermiso = [
    '#nombre',
    '#tiempo'
];

function validarCamposPermiso(){
    var i = 0;
    for(i; i < formularioPermiso.length; i++){
        $(formularioPermiso[i]).val().length > 0 ? $(formularioPermiso[i]).removeClass("is-invalid").addClass("is-valid") : $(formularioPermiso[i]).removeClass("is-valid").addClass("is-invalid");
    }
}

function limpiarCamposPermiso(){
    var i = 0;
    for(i; i < formularioPermiso.length; i++){
        $(formularioPermiso[i]).removeClass("is-invalid").removeClass("is-valid");
    }
    document.getElementById("formPermisos").reset();
}

function siguiente(){
    $('#home-tab').attr('aria-selected', false);
    $('#home-tab').attr({class: "nav-link"});
    $('#home').attr({class: "tab-pane fade content-form-general"})

    $('#profile-tab').attr('aria-selected', true);
    $('#profile-tab').attr({class: "nav-link active"});
    $('#profile').attr({class: "tab-pane fade content-form-trabajo active show"})
}

function anterior(){
    $('#profile-tab').attr('aria-selected', false);
    $('#profile-tab').attr({class: "nav-link"});
    $('#profile').attr({class: "tab-pane fade content-form-general"})

    $('#home-tab').attr('aria-selected', true);
    $('#home-tab').attr({class: "nav-link active"});
    $('#home').attr({class: "tab-pane fade content-form-trabajo active show"})
}