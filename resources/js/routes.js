function home() {
    $('#escritorio').load('resources/views/home.php');
}

function empleados() {
    $('#escritorio').load('resources/views/empleados.php');
}

function jornadasLaborales() {
    $('#escritorio').load('paginacion-jornada.php');
}

function tipoPermisos() {
    $('#escritorio').load('resources/views/tipoDePermisos.php');
}

function nuevoEmpleado() {
    $('#escritorio').load('resources/views/formNEEmpleado.php');
}

function nuevaJornada(){
    $('#escritorio').load('resources/views/formNEJornada.php');
}