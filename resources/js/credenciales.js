function buscar() {
    $.ajax({
        url: "buscar.php",
        method: "POST",
        dataType: "json",
        data: { funcion: "buscar", isss: $('#usuario').val() },
        success: function(respuesta) {
            $('#nombre').val(respuesta['nombres'] + " " + respuesta['apellidos']);
            var img = $(this);
            img.attr("delaySrc", img.attr("src"));
            $("#img").attr("src", respuesta['img']);
            $("#id").val(respuesta["id"]);
        }
    });
}

function std() {
    if ($('#estado').val() == 0) {
        $('#estado').val('1')
    } else if ($('#estado').val() == 1) {
        $('#estado').val('0')
    }
}