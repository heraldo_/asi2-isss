$(document).ready(function() {
    $.ajax({
        url: "pdm.php",
        method: "POST",
        data: { funcion: "init" },
        success: function(respuesta) {
            $('#pais').html(respuesta);
        }
    });
});

$(document).ready(function() {
    $("#pais").change(function() {
        $.ajax({
            url: "pdm.php",
            method: "POST",
            data: { data: $('#pais').val(), funcion: "departamento" },
            success: function(respuesta) {
                $('#departamento').html(respuesta);
                $('#ciudad').html("<option value=>Seleccione una ciudad</option>");
            }
        });
    });
});

$(document).ready(function() {
    $("#departamento").change(function() {
        $.ajax({
            url: "pdm.php",
            method: "POST",
            data: { data: $('#departamento').val(), funcion: "municipio" },
            success: function(respuesta) {
                $('#ciudad').html(respuesta);
            }
        });
    });
});