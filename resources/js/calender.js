$(function() {
    $("#calendario").datepicker();
});
$(function() {
    $("#fechaContrato").datepicker();
});

$('input[type=file]').change(function() {
    var file = (this.files[0].name).toString();
    var reader = new FileReader();

    $('#name-file').text(file);
    reader.onload = function(e) {
        $('#preview img').attr('src', e.target.result);
    }
    reader.readAsDataURL(this.files[0]);
});