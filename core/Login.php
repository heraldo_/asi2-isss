<?php
class Login{

    public function __construct(){
    
    } 

    public function init($POST){
      require_once 'Conexion.php';
      $conectar = new Conexion();
      $db = $conectar->conectar();

      $nickname = mysqli_real_escape_string($db,$POST['email']);
      $pass = mysqli_real_escape_string($db,$POST['contrasenia']);
      
      $sql = "SELECT * FROM cat_credenciales WHERE nickname = '$nickname' and pass = '$pass'";
      $result=$db->query($sql);
      $rows = $result->num_rows;
      
      if($rows > 0) {
        session_destroy();
        session_start();

        $row = $result->fetch_assoc();
        if(isset($POST['admin'])){
          if($POST['admin'] == $row['rol']){
            header("location: views/admin.php");
            $_SESSION['rol'] = $row['rol'];
          }
          else
            $_SESSION['PERMISO'] = "No tienes acceso permitido";
        }
        elseif(isset($POST['user'])){
          if($POST['user'] == $row['rol']){
            header("location: views/user.php");
            $_SESSION['rol'] = $row['rol'];
            $_SESSION['button'] = $POST['user'];
            $_SESSION['id'] = $row['fk_empleado'];
          }
          else
            $_SESSION['PERMISO'] = "No tienes acceso permitido";
        }
      } else {
        $_SESSION['ERROR'] = "El correo o la contraseña son incorrectos";
        
      }
    }
}
?>