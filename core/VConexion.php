<?php
class VConexion{
  private $driver;
  private $host, $user, $pass, $database;

  public function __construct(){
    $db_cfg = require '../config/database.php';
    $this->driver   =$db_cfg["driver"];
    $this->host     =$db_cfg["hostname"];
    $this->user     =$db_cfg["username"];
    $this->pass     =$db_cfg["password"];
    $this->database =$db_cfg["database"];
  }

  public function conectar(){ 
    if($this->driver=="mysql"){
        $con = new PDO('mysql:host='. $this->host .';dbname='. $this->database, $this->user,$this->pass);
    }
    return $con;
  }
}
?>