<?php
class Conexion{
  private $driver;
  private $host, $user, $pass, $database;

  public function __construct(){
    $db_cfg = require 'config/database.php';
    $this->driver   =$db_cfg["driver"];
    $this->host     =$db_cfg["hostname"];
    $this->user     =$db_cfg["username"];
    $this->pass     =$db_cfg["password"];
    $this->database =$db_cfg["database"];
  }

  public function conectar(){ 
    if($this->driver=="mysql"){
        $con = new mysqli($this->host, $this->user, $this->pass, $this->database);
    }
    return $con;
  }
}
?>