<?php
  
  require_once 'VConexion.php';
  
  class Jornadas{
    public function __construct(){
  }

    public function save($POST){
      $conectar = new VConexion();
      $db = $conectar->conectar();

      $sql = "INSERT INTO jornada (nombreJornada, descripcion, horaInicio, horaFinal)" . 
            "VALUES (
              '". $POST['nombre'] . "',
              '". $POST['descripcion'] ."',
              '". $POST['hora-inicio'] ."',
              '". $POST['hora-fin'] ."')";
      $result = $db->query($sql);
    }

    public function update($POST,$id){
      $conectar = new VConexion();
      $db = $conectar->conectar();
    
      $sql = "UPDATE `jornada` set 
              `nombreJornada`='".$POST['nombre']."',
              `descripcion`='".$POST['descripcion']."',
              `horaInicio`='".$POST['hora-inicio']."',
              `horaFinal`='".$POST['hora-fin']."' WHERE `id-jornada` =".$id;
      $result = $db->query($sql);
    }

    public function show($id){
      $conectar = new VConexion();
      $db = $conectar->conectar();

      $sql = $db->prepare("SELECT * FROM `jornada` WHERE `id-jornada`=".$id);
      $sql->execute();
      $resultado = $sql->fetchAll();

      return $resultado[0];
    }

    public function delete($id){
      $conectar = new VConexion();
      $db = $conectar->conectar();

      $sql = $db->prepare("DELETE FROM `jornada` WHERE `id-jornada`=".$id);
      $sql->execute();
    }
  }
?>