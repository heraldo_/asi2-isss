<?php
require_once 'VConexion.php';
   
class Empleado{
    public function __construct(){
  }

    public function get_jornadas(){
      $conectar = new VConexion();
      $db = $conectar->conectar();

      $sql = $db->prepare("SELECT `id-jornada`, `nombreJornada` FROM `jornada`");
      $sql->execute();
      $resultado = $sql->fetchAll();

      return $resultado;
    }

    public function save($POST){
        $conectar = new VConexion();
        $db = $conectar->conectar();

        $ruta_carpeta = "../resources/img/usuarios/";
        $nombre_archivo = "imagen_".date("dHis") .".". pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
        $ruta_guardar_archivo = $ruta_carpeta . $nombre_archivo;
        move_uploaded_file($_FILES["file"]["tmp_name"],$ruta_guardar_archivo);
  
        $sql = "INSERT INTO empleado (apellidos, nombres, estadoCivil, pais, 
                fechaDeNacimiento, departamento, telefono, ciudad,
                correo, dui, nit, afp, isss, direccion, contrato, fechaContrato,
                jornada, cargo, salario, dependencia, imagen)" . 
              "VALUES (
                '". $POST['apellidos'] . "',
                '". $POST['nombres'] ."',
                '". $POST['estadocivil'] ."',
                '". $POST['pais'] ."',
                '". $POST['fechanacimiento'] . "',
                '". $POST['departamento'] . "',
                '". $POST['telefono'] . "',
                '". $POST['ciudad'] . "',
                '". $POST['correo'] . "',
                '". $POST['dui'] . "',
                '". $POST['nit'] . "',
                '". $POST['afp'] . "',
                '". $POST['isss'] . "',
                '". $POST['direccion'] . "',
                '". $POST['contrato'] . "',
                '". $POST['fechaContrato'] . "',
                '". $POST['jornada'] . "',
                '". $POST['cargo'] . "',
                '". $POST['salario'] . "',
                '". $POST['dependencia'] . "',
                '". $ruta_guardar_archivo ."')";
        
        $result = $db->query($sql);
        return $sql;
    }

    public function update($POST,$id){
      $conectar = new VConexion();
      $db = $conectar->conectar(); 

      $ruta_carpeta = "../resources/img/usuarios/";
      $nombre_archivo = "imagen_".date("dHis") .".". pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
      $ruta_guardar_archivo = $ruta_carpeta . $nombre_archivo;
      move_uploaded_file($_FILES["file"]["tmp_name"],$ruta_guardar_archivo);

      $sql = "UPDATE `empleado` set 
              `apellidos`='".$POST['apellidos']."',
              `nombres`='".$POST['nombres']."',
              `estadoCivil`='".$POST['estadocivil']."',
              `pais`='".$POST['pais']."',
              `fechaDeNacimiento`='".$POST['fechanacimiento']."',
              `departamento`='".$POST['departamento']."',
              `telefono`='".$POST['telefono']."',
              `ciudad`='".$POST['ciudad']."',
              `correo`='".$POST['correo']."',
              `dui`='".$POST['dui']."',
              `nit`='".$POST['nit']."',
              `afp`='".$POST['afp']."',
              `isss`='".$POST['isss']."',
              `direccion`='".$POST['direccion']."',
              `contrato`='".$POST['contrato']."',
              `fechaContrato`='".$POST['fechaContrato']."',
              `jornada`='".$POST['jornada']."',
              `cargo`='".$POST['cargo']."',
              `salario`='".$POST['salario']."',
              `dependencia`='".$POST['dependencia']."',
              `imagen`='".$ruta_guardar_archivo."' WHERE `idEmpleado` =".$id;
      $result = $db->query($sql);
      return 0;
    }

    public function show($id){
      $conectar = new VConexion();
      $db = $conectar->conectar();

      $sql = $db->prepare("SELECT e.*, j.nombreJornada, j.`id-jornada`,j.horaInicio,j.horaFinal, p.nombrePais, d.nombreDepartamento, m.nombreMunicipio
      FROM empleado e, jornada j, cat_pais p, cat_departamento d, cat_municipio m
      WHERE e.jornada = j.`id-jornada` and p.idPais = e.pais and d.idDepartamento = e.departamento and m.idMunicipio = e.ciudad and e.idEmpleado =".$id);
      $sql->execute();
      $resultado = $sql->fetchAll();
      return $resultado[0];
    }

    public function delete($id){
      $conectar = new VConexion();
      $db = $conectar->conectar();

      $sql = $db->prepare("UPDATE `empleado` SET `activo`='N'  WHERE `idEmpleado`=".$id);
      $sql->execute();
      $resultado = $sql->fetchAll();
    }
}
?>