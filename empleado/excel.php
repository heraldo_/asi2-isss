<?php
	require "../core/Empleado.php";
	session_start();

	header("Content-Type: Application/xls");
	header("Content-Disposition: attachment; filename=reporteempleado.xls");

	$data = new Empleado;
	$id  = $_SESSION['idemp'];
	$row = $data->show($id);
?>
<div class="usr-table">
	<table class="table table-striped">
		<tr>
		<th>ID</th>
		<th>Apellidos</th>
		<th>Nombres</th>
		<th>Cargo</th>
		<th>DUI</th>
		<th>NIT</th>
		<th>AFP</th>
		<th>ISSS</th>
		<th>Estado Civil</th>
		<th>Correo</th>
		<th>Telefono</th>
		<th>Jornada</th>
		<th>Salario</th>
		<th>Fecha de contratacion</th>
		<th>Direccion</th>
		<th>Municipio</th>
		</tr>
	 	<tr>
			<td><?php echo $row['idEmpleado'] ?></td>
			<td><?php echo $row['apellidos'] ?></td>
			<td><?php echo $row['nombres'] ?></td>
			<td><?php echo $row['cargo'] ?></td>
			<td><?php echo $row['dui'] ?></td>
			<td><?php echo $row['nit'] ?></td>
			<td><?php echo $row['afp'] ?></td>
			<td><?php echo $row['isss'] ?></td>
			<td><?php echo $row['estadoCivil'] ?></td>
			<td><?php echo $row['correo'] ?></td>
			<td><?php echo $row['telefono'] ?></td>
			<td><?php echo $row['jornada'] ?></td>                  
			<td><?php echo "$".$row['salario'] ?></td>
			<td><?php echo $row['fechaContrato'] ?></td>
			<td><?php echo $row['direccion'] ?></td>
			<td><?php echo $row['dependencia'] ?></td>
		</tr>
	</table>
</div>
