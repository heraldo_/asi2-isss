<?php
require "../core/VConexion.php";
    $conectar = new VConexion();
    $db = $conectar->conectar();
    
if(isset($_POST['funcion']) && !empty($_POST['funcion'])) {
    if($_POST['funcion'] == "init"){
        $sql = $db->prepare("select * from cat_pais");
        $sql->execute();
        $resultado = $sql->fetchAll();
        $cadena = "<option value=>Seleccione un pais</option>";
        foreach ($resultado as $v) {
            $cadena=$cadena.'<option value="'.$v['idPais'].'">'. utf8_encode($v['nombrePais']).'</option>';
        }
        echo $cadena;
    }
    elseif($_POST['funcion'] == "departamento"){
        $sql = $db->prepare("select nombreDepartamento, idDepartamento from cat_departamento where fk_pais =" . $_POST['data']);
        $sql->execute();
        $resultado = $sql->fetchAll();
        $cadena = "<option value=>Seleccione un departamento</option>";
        foreach ($resultado as $v) {
            $cadena=$cadena.'<option value="'.$v['idDepartamento'].'">'. utf8_encode($v['nombreDepartamento']).'</option>';
        }
        echo $cadena;  
    }
    elseif($_POST['funcion'] == "municipio"){
        $sql = $db->prepare("select nombreMunicipio, idMunicipio from cat_municipio where fk_departamento =" . $_POST['data']);
        $sql->execute();
        $resultado = $sql->fetchAll();
        $cadena = "<option value=>Seleccione una ciudad</option>";
        foreach ($resultado as $v) {
            $cadena=$cadena.'<option value="'.$v['idMunicipio'].'">'. utf8_encode($v['nombreMunicipio']).'</option>';
        }
        echo $cadena;
    }
    
}
?>