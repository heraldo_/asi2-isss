<?php 
    require "../core/Empleado.php";

    $data = new Empleado;
    $info = $data->get_jornadas();

    $id = $_GET['id'];
    $resultado = $data->show($id);

    if(!isset($resultado)){
        header("location: home.php");
    }

    if(isset($_POST['actualizar-empleado'])){
        $data->update($_POST, $id);
        header("location: home.php");
    }

    require '../views/head-admin.php'; 
?>

<link rel="stylesheet" href="../resources/jquery-ui/jquery-ui.css">
<div class="formulario-empleado-nuevo">
    <br>
    <button class="btn btn-warning" onclick="limpiar()"> Limpiar </button>
    <br>
    <br>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                aria-selected="true">General</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                aria-selected="false">Trabajo</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <!-- General -->
        <div class="tab-pane fade show active content-form-general" id="home" role="tabpanel"
            aria-labelledby="home-tab">
            <form id="general" method="post" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <div class="form-group" id="preview">
                            <img src="<?php echo $resultado['imagen'] ?>" alt="..."
                                class="img-thumbnail img-empleado">
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="file" required>
                            <label class="custom-file-label" for="validatedCustomFile" id="name-file">buscar
                                imagen...</label>
                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                        </div>
                    </div>
                    <div class="col-md-9 form-letf-file">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label>Apellidos</label>
                                <input type="text" class="form-control" id="apellidos"
                                    placeholder="Introduzca los pellidos" name="apellidos" required
                                    value="<?php echo $resultado['apellidos'] ?>">
                                <div class="valid-feedback">correcto!</div>
                                <div class="invalid-feedback">El apellido es requerido!</div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>Estado civil</label>
                                <input type="text" class="form-control" id="estadoCivil"
                                    placeholder="Ingrese estado civil" name="estadocivil" required
                                    value="<?php echo $resultado['estadoCivil'] ?>">
                                <div class="valid-feedback">correcto!</div>
                                <div class="invalid-feedback">El nombre es requerido!</div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label>Nombre</label>
                                <input type="text" class="form-control" id="nombres"
                                    placeholder="Introduzca los nombres" name="nombres" required
                                    value="<?php echo $resultado['nombres'] ?>">
                                <div class="valid-feedback">correcto!</div>
                                <div class="invalid-feedback">El correo es requerido!</div>
                            </div>
                            <!-- PAIS -->
                            <div class="form-group col-md-6 mb-3">
                                <label>Pais</label>
                                <select class="custom-select" id="pais" name="pais" require>
                                    <option value="<?php echo $resultado['pais'] ?>"><?php echo $resultado['pais'] ?></option>
                                    <option value="El Salvador">El Salvador</option>
                                </select>
                                <div class="valid-feedback">correcto!</div>
                                <div class="invalid-feedback">El país es requerido!</div>
                            </div>
                        </div>
                        <!-- FECHA DE NACIENTO -->
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label>Fecha de nacimiento</label>
                                <input type="text" class="form-control" id="calendario" name="fechanacimiento" required
                                value="<?php echo $resultado['fechaDeNacimiento'] ?>">
                                <div class="valid-feedback">correcto!</div>
                                <div class="invalid-feedback">La fecha de nacimiento es requerido!</div>
                            </div>
                            <div class="form-group col-md-6 mb-3">
                                <label>Departamento</label>
                                <select class="custom-select" id="departamento" name="departamento" required>
                                    <option value="<?php echo $resultado['departamento'] ?>"><?php echo $resultado['departamento'] ?></option>
                                    <option value="San Salvador">San Salvador</option>
                                    <option value="Santa Ana">Santa Ana</option>
                                    <option value="La Libertad">La Libertad</option>
                                </select>
                                <div class="valid-feedback">correcto!</div>
                                <div class="invalid-feedback">El departamento es requerido!</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label>Telefono</label>
                        <input type="text" class="form-control" id="telefono"
                            placeholder="Introduzca número de telefono" name="telefono" required
                            value="<?php echo $resultado['telefono'] ?>"
                            >
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">El telefono es requerido!</div>
                    </div>
                    <div class="form-group col-md-4 mb-3">
                        <label>Ciudad</label>
                        <select class="custom-select" id="ciudad" placeholder="Introduzca la ciudad" name="ciudad"
                            required>
                            <option value="<?php echo $resultado['ciudad'] ?>"><?php echo $resultado['ciudad'] ?></option>
                            <option value="San Salvador">San Salvador</option>
                            <option value="Santa Ana">Santa Ana</option>
                            <option value="La Libertad">La Libertad</option>
                        </select>
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">La ciudad es requerida!</div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label>Correo</label>
                        <input type="text" class="form-control" id="correo" placeholder="Introduzca el correo"
                            name="correo" required value="<?php echo $resultado['correo'] ?>">
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">El correo es requerido!</div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label>DUI</label>
                        <input type="text" class="form-control" id="dui" placeholder="Introduzca numero de DUI"
                            name="dui" required value="<?php echo $resultado['dui'] ?>">
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">El DUI es requerido!</div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label>NIT</label>
                        <input type="text" class="form-control" id="nit" placeholder="Ingrese el NIT" name="nit"
                            required value="<?php echo $resultado['nit'] ?>">
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">El NIT es requerido!</div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label>AFP</label>
                        <input type="text" class="form-control" id="afp" placeholder="Ingrese AFP" name="afp" required
                        value="<?php echo $resultado['afp'] ?>">
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">El AFP es requerido!</div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label>Codigo ISSS</label>
                        <input type="text" class="form-control" id="isss" placeholder="Introduzca numero de ISSS"
                            name="isss" required value="<?php echo $resultado['isss'] ?>">
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">El ISSS es requerido!</div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label>Dirección</label>
                        <input type="text" class="form-control" id="direccion" placeholder="Ingrese la dirección"
                            name="direccion" required value="<?php echo $resultado['direccion'] ?>">
                        <div class="valid-feedback">correcto!</div>
                        <div class="invalid-feedback">La dirección es requerida!</div>
                    </div>
                </div>
                <div class="botones-form">
                    <a class="btn btn-danger cancelar" href="home">Cancelar</a>
                    <input type="button" class="btn btn-primary siguiente" value="Siguiente" onclick="siguiente()">
                </div>
        </div>
        <!-- Fin General -->
        <!-- Trabajo -->
        <div class="tab-pane fade content-form-trabajo" id="profile" role="tabpanel" aria-labelledby="profile-tab">

            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <div class="form-group" id="preview">
                        <img src="<?php echo $resultado['imagen'] ?>" alt="..." class="img-thumbnail img-empleado">
                    </div>
                </div>
                <div class="col-md-9 form-letf-file">
                    <div class="form-row">
                        <div class="form-group col-md-6 mb-3">
                            <label>Tipo de contratación</label>
                            <select class="custom-select" id="contrato" name="contrato" required>
                                <option value="<?php echo $resultado['nombreJornada'] ?>"><?php echo $resultado['nombreJornada'] ?></option>
                                <?php foreach ($info as $key): ?>
                                <option value="<?php echo $key['id-jornada']?>"><?php echo $key['nombreJornada']?>
                                </option>
                                <?php endforeach ?>
                            </select>
                            <div class="valid-feedback">correcto!</div>
                            <div class="invalid-feedback">El tipo de contrato es requerida!</div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Fecha de contratación</label>
                            <input type="text" class="form-control" id="fechaContrato" name="fechaContrato" required
                            value="<?php echo $resultado['fechaContrato'] ?>">
                            <div class="valid-feedback">correcto!</div>
                            <div class="invalid-feedback">La fecha de contratación es requerido!</div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 mb-3">
                            <label>Jornada</label>
                            <select class="custom-select" id="jornada" name="jornada" required>
                                <option value="<?php echo $resultado['id-jornada'] ?>"><?php echo $resultado['nombreJornada'] ?></option>
                                <?php foreach ($info as $key): ?>
                                <option value="<?php echo $key['id-jornada']?>"><?php echo $key['nombreJornada']?>
                                </option>
                                <?php endforeach ?>
                            </select>
                            <div class="valid-feedback">correcto!</div>
                            <div class="invalid-feedback">La jornada es requerida!</div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Cargo</label>
                            <input type="text" class="form-control" id="cargo" name="cargo"
                                placeholder="Ingrese el cargo" required value="<?php echo $resultado['cargo'] ?>">
                            <div class="valid-feedback">correcto!</div>
                            <div class="invalid-feedback">El cargo es requerido!</div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label>Salario</label>
                            <input type="text" class="form-control" id="salario" placeholder="Ingrese el salario"
                                name="salario" required value="<?php echo $resultado['salario'] ?>">
                            <div class="valid-feedback">correcto!</div>
                            <div class="invalid-feedback">El salario es requerido!</div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label>Dependencia</label>
                            <input type="text" class="form-control" id="dependencia"
                                placeholder="Ingrese la dependencia" name="dependencia" required
                                value="<?php echo $resultado['dependencia'] ?>">
                            <div class="valid-feedback">correcto!</div>
                            <div class="invalid-feedback">La dependencia es requerida!</div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="botones-form">
                    <input class="btn btn-primary" type="button" value="Anterior" onclick="anterior()">
                    <button class="btn btn-success cancelar" type="submit" onclick="validarCampos()"
                        name="actualizar-empleado">Actualizar</button>
                </div>
            </div>
            </form>
        </div>
        <!-- Fin Trabajo -->

    </div>
</div>
<?php require '../views/footer-admin.php'; ?>
<script src="../resources/js/pdm.js"></script>