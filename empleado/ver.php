<?php 
require '../views/head-admin.php'; 
require "../core/Empleado.php";

    $data = new Empleado;
    $id = $_GET['id'];
    $resultado = $data->show($id);
    
    $_SESSION['idemp'] = $resultado['idEmpleado'];
?>  

<div class="profile">
    <div>
        <img src="<?php echo $resultado['imagen'] ?>" alt="..." class="img-thumbnail ver-img">
    </div>
    <br>
    <div class="dt-info">
        <span class="ver-nmbr"><?php echo $resultado['nombres'] ." " . $resultado['apellidos'] ?></span>
        <p class="usr-cargo"><?php echo $resultado['cargo'] ?></p>
        <div>
            <i class="fas fa-map-marker-alt"></i>
            <span><?php echo $resultado['nombreMunicipio'] .", " . $resultado['nombreDepartamento']. ", ". $resultado['nombrePais'] ?></span>
        </div>
        <br>
        <p class="usr-cargo"><a class="btn btn-warning" href="reporte.php?id=<?php echo $resultado['idEmpleado'] ?>" target="_blank">Reporte</a></p>
    </div>
</div>
<br>
<div class="usr-table">
    <div class="col-sm-6">
        <p class="usr-titulo">Información Básica</p>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td scope="row">DUI</td>
                    <td><?php echo $resultado['dui'] ?></td>
                </tr>
                <tr>
                    <td scope="row">NIT</td>
                    <td><?php echo $resultado['nit'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Estado civil</td>
                    <td><?php echo $resultado['estadoCivil'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Correo</td>
                    <td><?php echo $resultado['correo'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Direccion</td>
                    <td><?php echo $resultado['direccion'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Telefono</td>
                    <td><?php echo $resultado['telefono'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-6 info-contrato">
        <p class="usr-titulo">Información de Contrato</p>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td scope="row">Turno</td>
                    <td><?php echo $resultado['nombreJornada'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Salario</td>
                    <td><?php echo "$".$resultado['salario'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Fecha de ingreso</td>
                    <td><?php echo $resultado['fechaContrato'] ?></td>
                </tr>
                <tr>
                    <td scope="row">Cargo</td>
                    <td><?php echo $resultado['cargo'] ?></td>
                </tr>
                <tr>
                    <td scope="row">AFP</td>
                    <td><?php echo $resultado['afp'] ?></td>
                </tr>
                <tr>
                    <td scope="row">ISSS</td>
                    <td><?php echo $resultado['isss'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="usr-btn">
    <a class="btn btn-success" href="home.php">Aceptar</a>
    &nbsp;
    <form method="post" action="excel.php">
    <input type="submit" name="exportar_excel" class="btn btn-info" value="Exportar">
    </form>
</div>

<?php require '../views/footer-admin.php'; ?>
