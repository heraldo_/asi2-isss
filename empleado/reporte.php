<?php 
require "../core/Empleado.php";

    $data = new Empleado;
    $id = $_GET['id'];
    $resultado = $data->show($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte</title>
</head>
<body style="font-family:consolas; font-size: 13px;">
    ------------------------------------------------------------------------------
    <br>
    <b>INSTITUTO SALVADORE&Ntilde;O DEL SEGURO SOCIAL</b>
    <br>
    ------------------------------------------------------------------------------
    <br>
    &nbsp;&nbsp;Nombre empleado&nbsp;&nbsp;:&nbsp;<?php echo $resultado['nombres'] ." " . $resultado['apellidos'] ?><br>
    &nbsp;&nbsp;Cargo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['cargo'] ?><br>
    &nbsp;&nbsp;Ubicaci&oacute;n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['nombreDepartamento'] .", " . $resultado['nombreMunicipio']. ", ". $resultado['nombrePais'] ?><br>
    <br>
    <b>Informaci&oacute;n B&aacute;sica</b><br>
    &nbsp;&nbsp;DUI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['dui'] ?><br>
    &nbsp;&nbsp;NIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['nit'] ?><br>
    &nbsp;&nbsp;Estado civil&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['estadoCivil'] ?><br>
    &nbsp;&nbsp;Correo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['correo'] ?><br>
    &nbsp;&nbsp;Direcci&oacute;n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['direccion'] ?><br>
    &nbsp;&nbsp;Tel&eacute;fono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['telefono'] ?><br>
    <br>
    <b>Informaci&oacute;n de Contrato</b><br>
    &nbsp;&nbsp;Turno&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['nombreJornada'] ?><br>
    &nbsp;&nbsp;Salario&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo "$".$resultado['salario'] ?><br>
    &nbsp;&nbsp;Fecha de ingreso&nbsp;:&nbsp;<?php echo $resultado['fechaContrato'] ?><br>
    &nbsp;&nbsp;Cargo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['cargo'] ?><br>
    &nbsp;&nbsp;AFP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['afp'] ?><br>
    &nbsp;&nbsp;ISSS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $resultado['isss'] ?><br>
    <script src="../resources/js/jquery.min.js"></script>
    <script>
        $( document ).ready(function() {
            window.print();
            window.open('','_parent',''); 
            window.close();
        });
    </script>
</body>
</html>